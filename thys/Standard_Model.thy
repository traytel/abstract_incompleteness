(* Abstract notion of standard model and truth  *)
theory Standard_Model imports Abstract_Representability
begin 


(* Truth in a standard model *)
(* First some minimal assumptions, involving only imp and all: *)
locale Minimal_Truth = 
Syntax_with_Numerals_and_Connectives_False_Disj 
  var trm fmla Var FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  dsj
  num
for 
var :: "'var set" and trm :: "'trm set" and fmla :: "'fmla set" 
and Var FvarsT substT Fvars subst  
and eql cnj imp all exi 
and fls
and dsj
and num  
+
(* The notion of truth for sentences: *)
fixes isTrue :: "'fmla \<Rightarrow> bool"
assumes   
not_isTrue_fls: "\<not> isTrue fls"
and 
isTrue_imp: 
"\<And>\<phi> \<psi>. \<phi> \<in> fmla \<Longrightarrow> \<psi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> Fvars \<psi> = {} \<Longrightarrow> 
  isTrue \<phi> \<Longrightarrow> isTrue (imp \<phi> \<psi>) \<Longrightarrow> isTrue \<psi>"
and
isTrue_all: 
"\<And>x \<phi>. x \<in> var \<Longrightarrow> \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> \<subseteq> {x} \<Longrightarrow> 
  (\<forall> n \<in> num. isTrue (subst \<phi> n x)) \<Longrightarrow> isTrue (all x \<phi>)"
and 
isTrue_exi: 
"\<And>x \<phi>. x \<in> var \<Longrightarrow> \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> \<subseteq> {x} \<Longrightarrow> 
  isTrue (exi x \<phi>) \<Longrightarrow> (\<exists> n \<in> num. isTrue (subst \<phi> n x))"
and 
isTrue_neg: 
"\<And>\<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> 
  isTrue \<phi> \<or> isTrue (neg \<phi>)"
begin

lemma isTrue_neg_excl:
"\<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow>   
 isTrue \<phi> \<Longrightarrow> isTrue (neg \<phi>) \<Longrightarrow> False"
  using isTrue_imp not_isTrue_fls unfolding neg_def by auto

lemma isTrue_neg_neg: 
assumes "\<phi> \<in> fmla" "Fvars \<phi> = {}"
and "isTrue (neg (neg \<phi>))"
shows "isTrue \<phi>"
using assms isTrue_neg isTrue_neg_excl by fastforce

end (* context  Minimal_Truth *)

  

locale Minimal_Truth_Soundness = 
Minimal_Truth 
  var trm fmla Var FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  dsj
  num
  isTrue
+
Deduct_with_False_Disj
  var trm fmla Var FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  dsj
  num
  prv 
for 
var :: "'var set" and trm :: "'trm set" and fmla :: "'fmla set" 
and Var FvarsT substT Fvars subst  
and eql cnj imp all exi 
and fls
and dsj
and num
and prv
and isTrue 
+
assumes 
(* We assume soundness of the provability for sentences (w.r.t. truth): *)
prv_sound_isTrue: 
"\<And>\<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> 
  prv \<phi> \<Longrightarrow> isTrue \<phi>"
begin

(* For sound theories, consistency is a fact rather than a hypothesis *)
lemma consistent: consistent
  unfolding consistent_def using not_isTrue_fls prv_sound_isTrue by blast

lemma prv_neg_excl:
"\<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> 
  prv \<phi> \<Longrightarrow> prv (neg \<phi>) \<Longrightarrow> False"
  using isTrue_neg_excl[of \<phi>] prv_sound_isTrue by auto

lemma prv_imp_implies_isTrue: 
assumes [simp,intro!]: "\<phi> \<in> fmla" "\<chi> \<in> fmla" "Fvars \<phi> = {}" "Fvars \<chi> = {}"
and p: "prv (imp \<phi> \<chi>)" and i: "isTrue \<phi>"
shows "isTrue \<chi>" 
proof-
  have "isTrue (imp \<phi> \<chi>)" using p apply(intro prv_sound_isTrue) by auto
  thus ?thesis using assms isTrue_imp by blast
qed

(* Sound theories are not only consistent, but also \<omega>consistent 
(in the strong, intuitionistic sense): *)
lemma \<omega>consistent: \<omega>consistent 
unfolding \<omega>consistent_def proof (safe del: notI)
  fix \<phi> x assume 0[simp,intro]: "\<phi> \<in> fmla"  "x \<in> var" and 1: "Fvars \<phi> \<subseteq> {x}"
  and 00: "\<forall>n\<in>num. prv (neg (subst \<phi> n x))"
  hence "\<forall>n\<in>num. isTrue (neg (subst \<phi> n x))" 
    using 00 1 apply(auto intro!: prv_sound_isTrue simp: Fvars_subst) apply fastforce 
    by (cases "x \<in> Fvars \<phi>") auto 
  hence "isTrue (all x (neg \<phi>))" by (simp add: "1" isTrue_all) 
  moreover 
  {have "prv (imp (all x (neg \<phi>)) (neg (exi x \<phi>)))"  
    using prv_all_neg_imp_neg_exi by blast
   hence "isTrue (imp (all x (neg \<phi>)) (neg (exi x \<phi>)))"
    by (simp add: "1" prv_sound_isTrue)
  }
  ultimately have "isTrue (neg (exi x \<phi>))"  
    by (metis 0 1 Diff_eq_empty_iff Fvars_all Fvars_exi 
         Fvars_neg all exi isTrue_imp neg)    
  hence "\<not> isTrue (neg (neg (exi x \<phi>)))"  
    using 1 isTrue_neg_excl by force 
  thus "\<not> prv (neg (neg (exi x \<phi>)))"  
    using "1" prv_sound_isTrue by auto 
qed 

lemma \<omega>consistentStd1: \<omega>consistentStd1
  using \<omega>consistent \<omega>consistent_impliesStd1 by blast

lemma \<omega>consistentStd2: \<omega>consistentStd2
  using \<omega>consistent \<omega>consistent_impliesStd2 by blast

end 


locale Minimal_Truth_Soundness_Proof_Repr = 
Repr_Proofs 
  var trm fmla Var FvarsT substT Fvars subst
  num
  eql cnj imp all exi 
  prv
  enc
  fls
  dsj 
  "proof" prfOf  
  encPf 
  Pf
+ 
Minimal_Truth_Soundness
  var trm fmla Var FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  dsj
  num
  prv
  isTrue
for 
var :: "'var set" and trm :: "'trm set" and fmla :: "'fmla set" 
and Var FvarsT substT Fvars subst  
and eql cnj imp all exi 
and fls
and dsj
and num
and prv 
and isTrue 
and enc ("\<langle>_\<rangle>") 
and "proof" :: "'proof set" and prfOf   
and encPf Pf 
begin

(* lemmas not_prv_not_prv_PPf = consistent_not_prv_not_prv_PPf[OF consistent] *)
lemmas prfOf_iff_PPf = consistent_prfOf_iff_PPf[OF consistent]

(* The provability predicate is decided by prv on encodings 
(just like with any predicate that "represents")   *)
lemma isTrue_prv_PPf_prf_or_neg: 
"prf \<in> proof \<Longrightarrow> \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> 
    prv (PPf (encPf prf) \<langle>\<phi>\<rangle>) \<or> prv (neg (PPf (encPf prf) \<langle>\<phi>\<rangle>))"
  using not_prfOf_PPf prfOf_PPf by blast

(* Hence that predicate is complete w.r.t. truth  *)
lemma isTrue_implies_prv_PPf_prf: 
"prf \<in> proof \<Longrightarrow> \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> 
   isTrue (PPf (encPf prf) \<langle>\<phi>\<rangle>) \<Longrightarrow> prv (PPf (encPf prf) \<langle>\<phi>\<rangle>)"
  by (metis FvarsT_num Fvars_PPf Fvars_fls PPf 
Un_empty empty_iff enc encPf fls in_num isTrue_prv_PPf_prf_or_neg 
neg_def not_isTrue_fls prv_imp_implies_isTrue) 

(* ... and thanks to cleanness we can replace encoded proofs 
with arbitrary numerals in the completeness property:  *)
lemma isTrue_implies_prv_PPf: 
assumes [simp]: "n \<in> num" "\<phi> \<in> fmla" "Fvars \<phi> = {}" 
and iT: "isTrue (PPf n \<langle>\<phi>\<rangle>)"
shows "prv (PPf n \<langle>\<phi>\<rangle>)" 
proof(cases "n \<in> encPf ` proof")
  case True  
  thus ?thesis  
    using iT isTrue_implies_prv_PPf_prf by auto  
next
  case False
  hence "prv (neg (PPf n \<langle>\<phi>\<rangle>))" by (simp add: PPf_encPf)
  hence "isTrue (neg (PPf n \<langle>\<phi>\<rangle>))" apply(intro prv_sound_isTrue) by auto
  hence False using iT by (intro isTrue_neg_excl) auto
  thus ?thesis by auto
qed

(* In fact, by soundness we even have an iff: *)
lemma isTrue_iff_prv_PPf: 
"\<And> n \<phi>. n \<in> num \<Longrightarrow> \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> isTrue (PPf n \<langle>\<phi>\<rangle>) \<longleftrightarrow> prv (PPf n \<langle>\<phi>\<rangle>)"
using isTrue_implies_prv_PPf  
using exists_no_Fvars not_isTrue_fls prv_sound_isTrue by auto

(*  Truth of the provability representation implies provability: *)
lemma isTrue_PP_implies_prv: 
assumes \<phi>[simp]: "\<phi> \<in> fmla" "Fvars \<phi> = {}"
and iPP: "isTrue (wrepr.PP \<langle>\<phi>\<rangle>)"
shows "prv \<phi>"
proof-
  have "isTrue (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>))" using iPP unfolding PP_PPf[OF \<phi>(1)] .
  from isTrue_exi[OF _ _ _ this] 
  obtain n where n[simp]: "n \<in> num" and "isTrue (PPf n \<langle>\<phi>\<rangle>)" by auto
  hence pP: "prv (PPf n \<langle>\<phi>\<rangle>)" using isTrue_implies_prv_PPf by auto
  hence "\<not> prv (neg (PPf n \<langle>\<phi>\<rangle>))"  
  using prv_neg_excl[of "PPf n \<langle>\<phi>\<rangle>"] by auto 
  then obtain "prf" where "prf"[simp]: "prf \<in> proof" and nn: "n = encPf prf"  
  using assms n PPf_encPf \<phi>(1) by blast
  have "prfOf prf \<phi>" using pP unfolding nn using prfOf_iff_PPf by auto
  thus ?thesis using prv_prfOf by auto 
qed

(* The reverse HBL1 (now without the \<omega>consistencyStd assumption which holds here 
thanks to our truth-in-standard-model assumption) *)


lemmas HBL1_rev = \<omega>consistentStd1_HBL1_rev[OF \<omega>consistentStd1]
(* Note: Would also follow by soundness from isTrue_PP_implies_prv *)

end (* Minimal_Truth_Soundness_Proof_Repr *)



(* Next: Proof recovery from HBL1_iff *)

locale Minimal_Truth_Soundness_HBL_iff = 
WRepr_Provability
  var trm fmla Var FvarsT substT Fvars subst
  num
  eql cnj imp all exi 
  prv 
  enc 
  P
+
Minimal_Truth_Soundness
  var trm fmla Var FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  dsj
  num
  prv
  isTrue
for 
var :: "'var set" and trm :: "'trm set" and fmla :: "'fmla set" 
and Var FvarsT substT Fvars subst  
and eql cnj imp all exi 
and fls
and dsj
and num
and enc ("\<langle>_\<rangle>") 
and prv 
and P
and isTrue
+ 
fixes Pf :: 'fmla
assumes
(* Pf is a formula with free variables xx yy  *)
Pf[simp,intro!]: "Pf \<in> fmla"
and 
Fvars_Pf[simp]: "Fvars Pf = {yy,xx}"
and 
(* P relates to Pf internally just like a prv and a proofOf would 
relate: via an existential *)
P_Pf:
"\<phi> \<in> fmla \<Longrightarrow>
 let PPf = (\<lambda> t1 t2. psubst Pf [(t1,yy), (t2,xx)]) in 
 prv (eqv (subst P \<langle>\<phi>\<rangle> xx) (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>)))"
assumes 
(* We assume both HLB and HBL_rev, i.e., an iff version: *)
HBL1_iff: "\<And> \<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> prv (PP \<langle>\<phi>\<rangle>) \<longleftrightarrow> prv \<phi>"
and 
isTrue_implies_prv_Pf: 
"\<And> n \<phi>. n \<in> num \<Longrightarrow> \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> 
 let PPf = (\<lambda> t1 t2. psubst Pf [(t1,yy), (t2,xx)]) in 
 isTrue (PPf n \<langle>\<phi>\<rangle>) \<longrightarrow> prv (PPf n \<langle>\<phi>\<rangle>)"
begin  
  
definition PPf where "PPf \<equiv> \<lambda> t1 t2. psubst Pf [(t1,yy), (t2,xx)]"

lemma PP_deff: "PP t = subst P t xx" using PP_def by auto

lemma PP_PPf_eqv: 
  "\<phi> \<in> fmla \<Longrightarrow> prv (eqv (PP \<langle>\<phi>\<rangle>) (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>)))"
  using PP_deff PPf_def P_Pf by auto

(*  *)
 
lemma PPf[simp,intro!]: "t1 \<in> trm \<Longrightarrow> t2 \<in> trm \<Longrightarrow> xx \<notin> FvarsT t1 \<Longrightarrow> PPf t1 t2 \<in> fmla"
  unfolding PPf_def by auto

lemma PPf_def2: "t1 \<in> trm \<Longrightarrow> t2 \<in> trm \<Longrightarrow> xx \<notin> FvarsT t1 \<Longrightarrow> 
  PPf t1 t2 = subst (subst Pf t1 yy) t2 xx"
  unfolding PPf_def apply(rule psubst_eq_rawpsubst2[simplified]) by auto

lemma Fvars_PPf[simp]: 
"t1 \<in> trm \<Longrightarrow> t2 \<in> trm \<Longrightarrow> xx \<notin> FvarsT t1 \<Longrightarrow> 
 Fvars (PPf t1 t2) = FvarsT t1 \<union> FvarsT t2"
by (auto simp add: PPf_def2 Fvars_subst subst2_fresh_switch)

lemma [simp]: 
"n \<in> num \<Longrightarrow> subst (PPf (Var yy) (Var xx)) n xx = PPf (Var yy) n"
"m \<in> num \<Longrightarrow> n \<in> num \<Longrightarrow> subst (PPf (Var yy) m) n yy = PPf n m"
"n \<in> num \<Longrightarrow> subst (PPf (Var yy) (Var xx)) n yy = PPf n (Var xx)"
"m \<in> num \<Longrightarrow> n \<in> num \<Longrightarrow> subst (PPf m (Var xx)) n xx = PPf m n"
"m \<in> num \<Longrightarrow> subst (PPf (Var zz) (Var xx')) m zz = PPf m (Var xx')"
"m \<in> num \<Longrightarrow> n \<in> num \<Longrightarrow> subst (PPf m (Var xx')) n xx' = PPf m n"
"n \<in> num \<Longrightarrow> subst (PPf (Var zz) (Var xx')) n xx' = PPf (Var zz) n"
"m \<in> num \<Longrightarrow> n \<in> num \<Longrightarrow> subst (PPf (Var zz) n) m zz = PPf m n"
  by (auto simp add: PPf_def2 Fvars_subst subst2_fresh_switch) 
    
(* *)

lemma PP_PPf: 
assumes "\<phi> \<in> fmla" shows "prv (PP \<langle>\<phi>\<rangle>) \<longleftrightarrow> prv (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>))"
  using assms PP_PPf_eqv[OF assms] prv_eqv_sym[OF _ _ PP_PPf_eqv[OF assms]]
  apply safe apply(rule prv_eqv_prv[of "PP \<langle>\<phi>\<rangle>" "exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>)"])
  apply simp_all 
  apply(rule prv_eqv_prv[of "exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>)" "PP \<langle>\<phi>\<rangle>" ]) by auto

lemma isTrue_implies_prv_PPf: 
"\<And> n \<phi>. n \<in> num \<Longrightarrow> \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow>  
 isTrue (PPf n \<langle>\<phi>\<rangle>) \<Longrightarrow> prv (PPf n \<langle>\<phi>\<rangle>)"
  using isTrue_implies_prv_Pf by(simp add: PPf_def)

lemma isTrue_iff_prv_PPf: 
"\<And> n \<phi>. n \<in> num \<Longrightarrow> \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> isTrue (PPf n \<langle>\<phi>\<rangle>) \<longleftrightarrow> prv (PPf n \<langle>\<phi>\<rangle>)"
using isTrue_implies_prv_PPf  
  using exists_no_Fvars not_isTrue_fls prv_sound_isTrue by auto



(*  *)
 
(* Preparing to instantiate this alternative into the mainstream locale hierarchy: 
We define the "missing" proofs to be numerals, we encode them as the identity, 
and we "copy" prfOf from the corresponding predicate one-level-up, PPf.
*) 
definition "proof" :: "'trm set" where [simp]: "proof = num"
definition prfOf :: "'trm \<Rightarrow> 'fmla \<Rightarrow> bool" where   
  "prfOf n \<phi> \<equiv> prv (PPf n \<langle>\<phi>\<rangle>)"  
definition encPf :: "'trm \<Rightarrow> 'trm" where [simp]: "encPf \<equiv> id"
(*  *)

lemma prv_exi_PPf_iff_isTrue:
assumes [simp]: "\<phi> \<in> fmla" "Fvars \<phi> = {}"  
shows "prv (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>)) \<longleftrightarrow> isTrue (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>))" (is "?L \<longleftrightarrow> ?R")
proof
  assume ?L thus ?R by (intro prv_sound_isTrue) auto
next
  assume ?R 
  obtain n where n[simp]: "n \<in> num" and i: "isTrue (PPf n \<langle>\<phi>\<rangle>)" using isTrue_exi[OF _ _ _ `?R`] by auto
  hence "prv (PPf n \<langle>\<phi>\<rangle>)" by (auto simp: isTrue_iff_prv_PPf)
  thus ?L by (intro prv_exiI[of _ _ n]) auto
qed

lemma isTrue_exi_iff:
assumes [simp]: "\<phi> \<in> fmla" "Fvars \<phi> = {}"  
shows "isTrue (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>)) \<longleftrightarrow> (\<exists>n\<in>num. isTrue (PPf n \<langle>\<phi>\<rangle>))" (is "?L \<longleftrightarrow> ?R")
proof
  assume ?L thus ?R using isTrue_exi[OF _ _ _ `?L`] by auto
next
  assume ?R
  then obtain n where n[simp]: "n \<in> num" and i: "isTrue (PPf n \<langle>\<phi>\<rangle>)" by auto
  hence "prv (PPf n \<langle>\<phi>\<rangle>)" by (auto simp: isTrue_iff_prv_PPf)
  hence "prv (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>))" by (intro prv_exiI[of _ _ n]) auto
  thus ?L by (intro prv_sound_isTrue) auto
qed

lemma prv_prfOf: 
assumes "\<phi> \<in> fmla" "Fvars \<phi> = {}"  
shows "prv \<phi> \<longleftrightarrow> (\<exists>n\<in>num. prfOf n \<phi>)"
proof-
  have "prv \<phi> \<longleftrightarrow> prv (PP \<langle>\<phi>\<rangle>)" using HBL1_iff[OF assms] by simp
  also have "\<dots> \<longleftrightarrow> prv (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>))" unfolding PP_PPf[OF assms(1)] ..
  also have "\<dots> \<longleftrightarrow> isTrue (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>))" using prv_exi_PPf_iff_isTrue[OF assms] .
  also have "\<dots> \<longleftrightarrow> (\<exists>n\<in>num. isTrue (PPf n \<langle>\<phi>\<rangle>))" using isTrue_exi_iff[OF assms] .
  also have "\<dots> \<longleftrightarrow> (\<exists>n\<in>num. prv (PPf n \<langle>\<phi>\<rangle>))" using isTrue_iff_prv_PPf[OF _ assms] by auto
  also have "\<dots> \<longleftrightarrow> (\<exists>n\<in>num. prfOf n \<phi>)" unfolding prfOf_def ..
  finally show ?thesis .
qed 

lemma prfOf_prv_Pf: 
assumes "n \<in> num" and "\<phi> \<in> fmla" "Fvars \<phi> = {}" and "prfOf n \<phi>"
shows "prv (psubst Pf [(n, yy), (\<langle>\<phi>\<rangle>, xx)])" 
using assms unfolding prfOf_def by (auto simp: PPf_def2 psubst_eq_rawpsubst2) 

lemma isTrue_exi_iff_PP:
assumes [simp]: "\<phi> \<in> fmla" "Fvars \<phi> = {}"  
shows "isTrue (PP \<langle>\<phi>\<rangle>) \<longleftrightarrow> (\<exists>n\<in>num. isTrue (PPf n \<langle>\<phi>\<rangle>))"  
proof-
  have "prv (eqv (PP \<langle>\<phi>\<rangle>) (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>)))" 
    using PP_PPf_eqv by auto
  hence "prv (imp (PP \<langle>\<phi>\<rangle>) (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>)))"
  and "prv (imp (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>)) (PP \<langle>\<phi>\<rangle>))"  
  by (simp_all add: prv_imp_eqvEL prv_imp_eqvER)   
  thus ?thesis unfolding isTrue_exi_iff[OF assms, symmetric] 
  apply safe apply(rule prv_imp_implies_isTrue) apply simp_all
  apply(rule prv_imp_implies_isTrue) by simp_all
qed

end (* context Minimal_Truth_Soundness_HBL_iff *)


locale Minimal_Truth_Soundness_HBL_iff_Variant_Neg = 
Minimal_Truth_Soundness_HBL_iff
+
assumes    
isTrue_implies_prv_neg_Pf: 
"\<And> n \<phi>. n \<in> num \<Longrightarrow> \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> 
 let PPf = (\<lambda> t1 t2. psubst Pf [(t1,yy), (t2,xx)]) in 
 isTrue (neg (PPf n \<langle>\<phi>\<rangle>)) \<longrightarrow> prv (neg (PPf n \<langle>\<phi>\<rangle>))" 
begin

lemma isTrue_implies_prv_neg_PPf: 
"\<And> n \<phi>. n \<in> num \<Longrightarrow> \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow>  
 isTrue (neg (PPf n \<langle>\<phi>\<rangle>)) \<Longrightarrow> prv (neg (PPf n \<langle>\<phi>\<rangle>))"
  using isTrue_implies_prv_neg_Pf by(simp add: PPf_def) 

lemma isTrue_iff_prv_neg_PPf: 
"\<And> n \<phi>. n \<in> num \<Longrightarrow> \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> isTrue (neg (PPf n \<langle>\<phi>\<rangle>)) \<longleftrightarrow> prv (neg (PPf n \<langle>\<phi>\<rangle>))"
using isTrue_implies_prv_neg_PPf  
  using exists_no_Fvars not_isTrue_fls prv_sound_isTrue by auto

lemma prv_PPf_decide: 
assumes [simp]: "n \<in> num" "\<phi> \<in> fmla" "Fvars \<phi> = {}"
and np: "\<not> prv (PPf n \<langle>\<phi>\<rangle>)"
shows "prv (neg (PPf n \<langle>\<phi>\<rangle>))"
proof-
  have "\<not> isTrue (PPf n \<langle>\<phi>\<rangle>)" using assms by (auto simp: isTrue_iff_prv_PPf)
  hence "isTrue (neg (PPf n \<langle>\<phi>\<rangle>))" using isTrue_neg[of "PPf n \<langle>\<phi>\<rangle>"] by auto
  thus ?thesis by (auto simp: isTrue_iff_prv_neg_PPf)
qed 

lemma not_prfOf_prv_neg_Pf: 
assumes n\<phi>: "n \<in> num" "\<phi> \<in> fmla" "Fvars \<phi> = {}" and "\<not> prfOf n \<phi>"
shows "prv (neg (psubst Pf [(n, yy), (\<langle>\<phi>\<rangle>, xx)]))" 
  using assms prv_PPf_decide[OF n\<phi>] by (auto simp: prfOf_def  PPf_def2 psubst_eq_rawpsubst2)

end (* context Minimal_Truth_Soundness_HBL_iff_Variant_\<Delta> *)

sublocale Minimal_Truth_Soundness_HBL_iff_Variant_Neg < repr: Repr_Proofs  
(* added label to avoid duplicated constant P, which is assumed 
in Minimal_Truth_Soundness_HBL_iff but defined in Repr_Proofs  
(they are of course extensionally equal).
*)
  where "proof" = "proof" and prfOf = prfOf and encPf = encPf
  apply standard
  apply (auto simp: prv_prfOf prfOf_prv_Pf not_prfOf_prv_neg_Pf)
  done

sublocale Minimal_Truth_Soundness_HBL_iff_Variant_Neg < min_truth: Minimal_Truth_Soundness_Proof_Repr
where "proof" = "proof" and prfOf = prfOf and encPf = encPf
  apply standard 
  done

locale Minimal_Truth_Soundness_HBL_iff_Variant_Cls = 
Minimal_Truth_Soundness_HBL_iff
+
assumes   
(*NB: we don't really need to assume classical reasoning (double negation) all throughout, 
but only for the provability predicate: *)
classic_P: "\<And> \<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> let PP = (\<lambda>t. subst P t xx) in 
  prv (neg (neg (PP \<langle>\<phi>\<rangle>))) \<Longrightarrow> prv (PP \<langle>\<phi>\<rangle>)"
begin

lemma classic_PP: "\<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> prv (neg (neg (PP \<langle>\<phi>\<rangle>))) \<Longrightarrow> prv (PP \<langle>\<phi>\<rangle>)"
  using classic_P unfolding PP_def by auto


end (* context Minimal_Truth_Soundness_HBL_iff_Variant_Classic *)



  


end 
