theory Abstract_First_Godel imports Godel_Formula Standard_Model
begin 

(* Here we prove half of (the abstract version of) Godel's first theorem, 
which only needs the fixpoint property of the Godel formula *)

context Godel_Form 
begin

(* First the "direct", positive formulation: *)
lemma godel_first_theEasyHalf_pos: 
assumes "prv \<phi>G"   shows "prv fls"
proof-
  have "prv (neg (PP \<langle>\<phi>G\<rangle>))" using prv_eqv_prv[OF _ _ assms prv_\<phi>G_eqv] by auto
  moreover have "prv (PP \<langle>\<phi>G\<rangle>)" using HBL1[OF \<phi>G Fvars_\<phi>G assms] unfolding PP_def .
  ultimately show ?thesis using PP prv_neg_fls by (meson \<phi>G enc in_num)
qed

(* Then the more standard counterpositive formulation: *)
corollary godel_first_theEasyHalf: 
"consistent \<Longrightarrow> \<not> prv \<phi>G"
using godel_first_theEasyHalf_pos unfolding consistent_def by auto
 


end (* context Godel_Form *)

(* The other half needs explicit proofs: *)
context Godel_Form_with_Proofs begin
  
lemma godel_first_theHardHalf: 
assumes oc: "\<omega>consistent"
shows "\<not> prv (neg \<phi>G)"
proof
  assume pn: "prv (neg \<phi>G)"
  hence pnn: "prv (neg (neg (wrepr.PP \<langle>\<phi>G\<rangle>)))"  
    using prv_eqv_imp_transi num wrepr.PP \<phi>G fls neg neg_def prv_\<phi>G_eqv prv_eqv_sym
    by (metis (full_types) enc in_num)
  note c = \<omega>consistent_implies_consistent[OF oc] 
  have np: "\<not> prv \<phi>G" using pn c unfolding consistent_def3 by blast
  hence 0: "\<forall>p \<in> num. prv (neg (PPf p \<langle>\<phi>G\<rangle>))" using not_prv_prv_neg_PPf[OF _ _ np] by auto
  have "\<not> prv (neg (neg (exi yy (PPf (Var yy) \<langle>\<phi>G\<rangle>))))" using 0 oc unfolding \<omega>consistent_def by auto
  hence "\<not> prv (neg (neg (wrepr.PP \<langle>\<phi>G\<rangle>)))" apply(simp add: wrepr.PP_def)  apply(subst P_def) by (simp add: PPf_def2)
  thus False using pnn by auto
qed

theorem godel_first: 
assumes "\<omega>consistent"
shows "\<not> prv \<phi>G \<and> \<not> prv (neg \<phi>G)"
  using assms godel_first_theEasyHalf godel_first_theHardHalf \<omega>consistent_implies_consistent by blast

theorem godel_first_ex: 
assumes "\<omega>consistent"
shows "\<exists> \<phi>. \<phi> \<in> fmla \<and> \<not> prv \<phi> \<and> \<not> prv (neg \<phi>)" 
  using assms godel_first by (intro exI[of _ \<phi>G]) blast


end (* context Godel_Form_with_Proofs *)


(********************************)
(* The truth of the Godel sentence \<phi>G in the standard model *)

locale Godel_Form_with_Proofs_and_Minimal_Truth = 
Godel_Form_with_Proofs 
  var trm fmla Var FvarsT substT Fvars subst
  num
  eql cnj imp all exi 
  fls 
  prv 
  enc
  S 
  dsj 
  "proof" prfOf encPf 
  Pf
+
Minimal_Truth_Soundness
  var trm fmla Var FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  dsj
  num
  prv 
  isTrue
for 
var :: "'var set" and trm :: "'trm set" and fmla :: "'fmla set" 
and Var FvarsT substT Fvars subst  
and eql cnj imp all exi 
and fls
and dsj
and num
and prv   
and enc ("\<langle>_\<rangle>")
and S 
and "proof" :: "'proof set" and prfOf encPf 
and Pf
and isTrue
begin 

(* \<phi>G is not only undecided by prv, but is also true in the standard model. 
(Note that this only works for sound theories, as stated in the Minimal_Truth locale assumptions)
 *)

theorem isTrue_\<phi>G:
"isTrue \<phi>G" 
proof-
  have "\<forall> n \<in> num. prv (neg (PPf n \<langle>\<phi>G\<rangle>))" 
  using not_prv_prv_neg_PPf[OF _ _ godel_first_theEasyHalf[OF consistent]] by auto
  hence "\<forall> n \<in> num. isTrue (neg (PPf n \<langle>\<phi>G\<rangle>))" by (auto intro: prv_sound_isTrue)
  hence "isTrue (all yy (neg (PPf (Var yy) \<langle>\<phi>G\<rangle>)))" by (auto intro: isTrue_all)
  moreover have "isTrue (imp (all yy (neg (PPf (Var yy) \<langle>\<phi>G\<rangle>))) \<phi>G)"
  using prv_eqv_all_not_PPf_imp_\<phi>G by (auto intro!: prv_sound_isTrue) 
  ultimately show ?thesis apply- by (rule isTrue_imp) auto
qed



(* Now we have (for sound theories) the strong form of Godel's first, which does 
not assume \<omega>consistency and also concludes the truth of the undecided formula: *)

theorem godel_first_strong: "\<not> prv \<phi>G \<and> \<not> prv (neg \<phi>G) \<and> isTrue \<phi>G"
  using godel_first[OF \<omega>consistent] isTrue_\<phi>G by simp

theorem godel_first_strong_ex: 
"\<exists> \<phi>. \<phi> \<in> fmla \<and> \<not> prv \<phi> \<and> \<not> prv (neg \<phi>) \<and> isTrue \<phi>" 
  using godel_first_strong by (intro exI[of _ \<phi>G]) blast

(* NB: Since \<omega>consistency (as well as consistency) is a consequence of soundness, 
it turns out that, fr sound theories, our locale for Godel first is in principle 
easier to instantiate than our locale for Godel-Rosser (which instead of \<omega>consistency asks 
for an order-like relation subject to two axioms). 
*)

end (*context Godel_Form_with_Proofs_and_Minimal_Truth *)


locale Godel_Form_Classic =  
Godel_Form
  var trm fmla Var num FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  prv 
  enc 
  S 
  P
for 
var :: "'var set" and trm :: "'trm set" and fmla :: "'fmla set" 
and Var num FvarsT substT Fvars subst
and eql cnj imp all exi
and fls
and prv
and enc ("\<langle>_\<rangle>")
and S
and P
+
assumes 
HBL1_rev: "\<And> \<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> prv (PP \<langle>\<phi>\<rangle>) \<Longrightarrow> prv \<phi>"
and 
(*NB: we don't really need to assume classical reasoning 
(double negation) all throughout, 
but only for the provability predicate: *)
classic_P: "\<And> \<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> let PP = (\<lambda>t. subst P t xx) in 
  prv (neg (neg (PP \<langle>\<phi>\<rangle>))) \<Longrightarrow> prv (PP \<langle>\<phi>\<rangle>)"
begin

lemma classic_PP: "\<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> prv (neg (neg (PP \<langle>\<phi>\<rangle>))) \<Longrightarrow> prv (PP \<langle>\<phi>\<rangle>)"
  using classic_P unfolding PP_def by auto

lemma HBL1_iff: "\<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> prv (PP \<langle>\<phi>\<rangle>) \<longleftrightarrow> prv \<phi>"
  using HBL1 HBL1_rev unfolding PP_def by auto
  
lemma godel_first_theHardHalf_pos: 
assumes "prv (neg \<phi>G)"   shows "prv fls"
proof-
  have "prv (neg (neg (PP \<langle>\<phi>G\<rangle>)))"  
    using assms neg_def prv_\<phi>G_eqv prv_eqv_imp_transi_rev by fastforce
  hence "prv (PP \<langle>\<phi>G\<rangle>)" using classic_PP by auto 
  hence "prv \<phi>G" using Fvars_\<phi>G HBL1_rev by blast
  thus ?thesis using assms prv_neg_fls by blast  
qed

(* Then the more standard counterpositive formulation: *)
corollary godel_first_theHardHalf: 
"consistent \<Longrightarrow> \<not> prv (neg \<phi>G)"
  using godel_first_theHardHalf_pos unfolding consistent_def by auto

theorem godel_first_classic: 
assumes "consistent"
shows "\<not> prv \<phi>G \<and> \<not> prv (neg \<phi>G)"
  using assms godel_first_theEasyHalf godel_first_theHardHalf by blast

theorem godel_first_classic_ex: 
assumes "consistent"
shows "\<exists> \<phi>. \<phi> \<in> fmla \<and> \<not> prv \<phi> \<and> \<not> prv (neg \<phi>)" 
  using assms godel_first_classic by (intro exI[of _ \<phi>G]) blast


end (* Godel_Form_Classic *)

(************************)
locale Godel_Form_Minimal_Truth_Soundness_HBL_iff = 
Godel_Form 
  var trm fmla Var num 
  FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  prv  
  enc
  S 
  P 
+  
Minimal_Truth_Soundness_HBL_iff
  var trm fmla Var FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  dsj
  num
  enc
  prv
  P
  isTrue
  Pf   
for 
var :: "'var set" and trm :: "'trm set" and fmla :: "'fmla set" 
and Var FvarsT substT Fvars subst  
and eql cnj imp all exi 
and fls
and dsj
and num
and prv   
and enc ("\<langle>_\<rangle>")
and S 
and isTrue
and P
and Pf


(**************************************)
(* To further obtain that the Godel formula is true, Classic takes advantage of soundness 
(w.r.t. a standard model) 
together with the fact that the truth of P \<langle>\<phi>\<rangle> implies the provability of \<phi>: 
*)

locale Godel_Form_Classic_soundness =  
Minimal_Truth_Soundness 
  var trm fmla Var FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  dsj
  num
  prv
  isTrue 
+
Godel_Form_Classic 
  var trm fmla Var num FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  prv 
  enc 
  S 
  P
for 
var :: "'var set" and trm :: "'trm set" and fmla :: "'fmla set" 
and Var num FvarsT substT Fvars subst
and eql cnj dsj imp all exi
and fls
and prv
and enc ("\<langle>_\<rangle>")
and S
and P
and isTrue
+
assumes 
isTrue_P_implies_prv: "\<And> \<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> 
  let PP = (\<lambda>t. subst P t xx) in 
  isTrue (PP \<langle>\<phi>\<rangle>) \<Longrightarrow> prv \<phi>"
begin

lemma isTrue_PP_implies_prv: "\<phi> \<in> fmla \<Longrightarrow>  Fvars \<phi> = {} \<Longrightarrow> isTrue (PP \<langle>\<phi>\<rangle>) \<Longrightarrow> prv \<phi>"
  using isTrue_P_implies_prv unfolding PP_def by auto

lemma isTrue_PP_implies_isTrue: "\<phi> \<in> fmla \<Longrightarrow>  Fvars \<phi> = {} \<Longrightarrow> isTrue (PP \<langle>\<phi>\<rangle>) \<Longrightarrow> isTrue \<phi>"
  using isTrue_PP_implies_prv prv_sound_isTrue by blast

theorem isTrue_\<phi>G: "isTrue \<phi>G" 
proof-
  {assume 0: "\<not> isTrue \<phi>G"
   hence "isTrue (neg \<phi>G)" using isTrue_neg by fastforce
   hence "isTrue (neg (neg (PP \<langle>\<phi>G\<rangle>)))" apply- apply(rule prv_imp_implies_isTrue) 
     by (auto simp add: prv_\<phi>G_eqv prv_imp_eqvER prv_imp_neg_rev)
   hence "isTrue (PP \<langle>\<phi>G\<rangle>)" apply- by (rule isTrue_neg_neg) auto
   hence "isTrue \<phi>G" apply- by (rule isTrue_PP_implies_isTrue) auto
   hence False using 0 by simp
  }
  thus ?thesis by auto
qed

theorem godel_first_strong: "\<not> prv \<phi>G \<and> \<not> prv (neg \<phi>G) \<and> isTrue \<phi>G"
  using godel_first_classic[OF consistent] isTrue_\<phi>G by simp

theorem godel_first_strong_ex: 
"\<exists> \<phi>. \<phi> \<in> fmla \<and> \<not> prv \<phi> \<and> \<not> prv (neg \<phi>) \<and> isTrue \<phi>" 
  using godel_first_strong by (intro exI[of _ \<phi>G]) blast 

end (* locale Godel_Form_Classic_soundness *)


locale Godel_Form_Minimal_Truth_Soundness_HBL_iff_Variant_Neg = 
Godel_Form_Minimal_Truth_Soundness_HBL_iff +
Minimal_Truth_Soundness_HBL_iff_Variant_Neg

locale Godel_Form_Minimal_Truth_Soundness_HBL_iff_Variant_Cls = 
Godel_Form_Minimal_Truth_Soundness_HBL_iff +
Minimal_Truth_Soundness_HBL_iff_Variant_Cls
begin

lemma isTrue_PP_implies_prv: 
assumes 1: "\<phi> \<in> fmla" "Fvars \<phi> = {}" and 2: "isTrue (PP \<langle>\<phi>\<rangle>)"
shows "prv \<phi>"
proof-
  obtain n where nn: "n \<in> num" and i: "isTrue (PPf n \<langle>\<phi>\<rangle>)" 
   using 2 unfolding isTrue_exi_iff_PP[OF 1] .. 
  hence "prv (PPf n \<langle>\<phi>\<rangle>)" using i using nn assms isTrue_iff_prv_PPf by blast
  hence "prv (exi yy (PPf (Var yy) \<langle>\<phi>\<rangle>))"  
  using 2 assms isTrue_exi_iff isTrue_exi_iff_PP prv_exi_PPf_iff_isTrue by auto
  hence "prv (PP \<langle>\<phi>\<rangle>)" using PP_PPf 1 by blast
  thus ?thesis using HBL1_iff assms by blast
qed

end (* context Godel_Form_Minimal_Truth_Soundness_HBL_iff_Variant_Cls *)
 
sublocale 
  Godel_Form_Minimal_Truth_Soundness_HBL_iff_Variant_Neg < 
  min_truth: Godel_Form_with_Proofs_and_Minimal_Truth 
where prfOf = prfOf and "proof" = "proof" and encPf = encPf 
by standard   

sublocale Godel_Form_Minimal_Truth_Soundness_HBL_iff_Variant_Cls < 
  cls: Godel_Form_Classic_soundness
apply standard 
  apply (simp add: HBL1_iff)
  using classic_P apply blast
  by (simp add: PP_def isTrue_PP_implies_prv)

(* The (corrected) paper's Theorem 11, in two variants: 
  (1) under assumption Soundness, Syn\<Turnstile>, ComplP, HBL1, HBL\<Leftarrow>1, ReprS and 
Compl\<not>P: *)
context Godel_Form_Minimal_Truth_Soundness_HBL_iff_Variant_Neg begin
thm min_truth.godel_first_strong 
end  
(* (2) under assumption Soundness, Syn\<Turnstile>, ComplP, HBL1, HBL\<Leftarrow>1, ReprS and 
"classical deduction": *)
context Godel_Form_Minimal_Truth_Soundness_HBL_iff_Variant_Cls begin
thm cls.godel_first_strong 
end 

end
