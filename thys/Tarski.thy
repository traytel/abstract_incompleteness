(* We prove Tarski's syntactic and semantic theorems about the non-definability 
and respectively non-expressiveness of truth *)

theory Tarski imports Godel_Formula Standard_Model
begin 


context Godel_Form
begin 

context 
  fixes T :: 'fmla
  assumes T[simp,intro!]: "T \<in> fmla"
  and Fvars_T[simp]: "Fvars T \<subseteq> {xx}"
  and prv_T: "\<And>\<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> \<subseteq> {} \<Longrightarrow> prv (eqv (subst T \<langle>\<phi>\<rangle> xx) \<phi>)"
begin

definition \<phi>T :: 'fmla where "\<phi>T \<equiv> diag (neg T)"

lemma \<phi>T[simp,intro!]: "\<phi>T \<in> fmla"
and
Fvars_\<phi>T[simp]: "Fvars \<phi>T = {}"  "Fvars \<phi>T \<subseteq> {}"
  unfolding \<phi>T_def PP_def by auto

lemma prv_\<phi>T_eqv: 
"prv (eqv \<phi>T (neg (subst T \<langle>\<phi>T\<rangle> xx)))"
  unfolding \<phi>T_def using prv_diag_eqv[of "neg T"] by simp
 
lemma \<phi>T_prv_fls: "prv fls" 
using prv_eqv_eqv_neg_prv_fls2[OF _ _ prv_T[OF \<phi>T Fvars_\<phi>T(2)] prv_\<phi>T_eqv] by auto

end (* context *)

theorem Tarski_syntactic: 
assumes "T \<in> fmla" "Fvars T \<subseteq> {xx}"
and "\<And>\<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> prv (eqv (subst T \<langle>\<phi>\<rangle> xx) \<phi>)"
shows "\<not> consistent"
using \<phi>T_prv_fls[OF assms] consistent_def by auto

end (* context Godel_Form *)


(* The semantic version of Tarski's theorem *)

(* This follows as a corollary of the syntactic version, after taking prv to be isTrue 
on sentences.
Indeed, this is a virtue of our abstract treatment of provability: We don't work with a 
particular predicate, but with any predicate that is closed under some rules --- which could as 
well be a semantic notion of truth (for sentences). 
*)


locale Godel_Form_prv_eq_isTrue = 
Godel_Form  
  var trm fmla Var num FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  prv 
  enc
  P
  S
for 
var :: "'var set" and trm :: "'trm set" and fmla :: "'fmla set" 
and Var num FvarsT substT Fvars subst
and eql cnj imp all exi
and fls
and prv
and enc ("\<langle>_\<rangle>")
and S
and P
+
fixes isTrue :: "'fmla \<Rightarrow> bool"
assumes prv_eq_isTrue: "\<And> \<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> prv \<phi> = isTrue \<phi>"
begin


theorem Tarski_semantic: 
assumes 0: "T \<in> fmla" "Fvars T \<subseteq> {xx}"
and 1: "\<And>\<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> isTrue (eqv (subst T \<langle>\<phi>\<rangle> xx) \<phi>)"
shows "\<not> consistent" 
using assms prv_eq_isTrue[of "eqv (subst T \<langle>_\<rangle> xx) _"] 
  by (intro Tarski_syntactic[OF 0])  (auto simp: Fvars_subst)


(* Note that, to instantiate the semantic version of Tarski's theorem for a truth predicate isTruth 
on sentences, one needs to extend it to a predicate "prv" on formulas and verify that "prv" satisfies 
the rules of intuitionistic classical logic.
*)
     
end (* context Godel_Form_prv_eq_isTrue *)





end 
