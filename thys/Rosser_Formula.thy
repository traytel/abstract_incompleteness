(* The Rosser formula \<phi>R, a modification of the Godel formula that 
is undecidable assuming consistency only (not \<omega>consistency)
*)

theory Rosser_Formula imports Diagonalization
begin  

 
locale Rosser_Form =  
Deduct_with_Proofs_PseudoOrder
  var trm fmla Var FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  dsj
  num
  prv
  "proof" prfOf
  Lq
+
Repr_Neg
  var trm fmla Var FvarsT substT Fvars subst
  eql cnj imp all exi 
  fls
  num
  prv
  enc 
  N
+
Repr_Subst
  var trm fmla Var FvarsT substT Fvars subst
  num
  eql cnj imp all exi 
  prv 
  enc
  S 
+
Repr_Proofs 
  var trm fmla Var FvarsT substT Fvars subst
  num
  eql cnj imp all exi 
  prv
  enc
  fls
  dsj 
  "proof" prfOf  
  encPf 
  Pf
for 
var :: "'var set" and trm :: "'trm set" and fmla :: "'fmla set" 
and Var num FvarsT substT Fvars subst  
and eql cnj imp all exi 
and fls
and prv
and Lq 
and dsj and "proof" :: "'proof set" and prfOf  
and enc ("\<langle>_\<rangle>")
and N
and S
and P
and encPf Pf
(*  *)
begin

definition R where "R \<equiv> all zz (imp (LLq (Var zz) (Var yy))
                                     (all xx' (imp (NN (Var xx) (Var xx')) 
                                                   (neg (PPf (Var zz) (Var xx'))))))"

definition RR where "RR \<equiv> \<lambda>t1 t2. psubst R [(t1,yy), (t2,xx)]"

lemma R[simp,intro!]: "R \<in> fmla" unfolding R_def by auto

lemma RR_def2:   
"t1 \<in> trm \<Longrightarrow> t2 \<in> trm \<Longrightarrow> xx \<notin> FvarsT t1 \<Longrightarrow> RR t1 t2 = subst (subst R t1 yy) t2 xx"
  unfolding RR_def apply(rule psubst_eq_rawpsubst2[simplified]) by auto

definition P' where "P' \<equiv> exi yy (cnj (PPf (Var yy) (Var xx)) 
                                      (RR (Var yy) (Var xx)))"

definition PP' where "PP' \<equiv> \<lambda>t. subst P' t xx"

(* TODO: here and elsewhere, more compact and uniform simps for the formula combinators *)

lemma Fvars_R[simp]: "Fvars R = {xx,yy}" unfolding R_def by auto

lemma [simp]: "Fvars (RR (Var yy) (Var xx)) = {yy,xx}" by (auto simp: RR_def2)

lemma P'[simp,intro!]: "P' \<in> fmla" unfolding P'_def by (auto simp: PPf_def2 RR_def2)

lemma Fvars_P'[simp]: "Fvars P' = {xx}" unfolding P'_def by (auto simp: PPf_def2 RR_def2)

lemma PP'[simp,intro!]: "t \<in> trm \<Longrightarrow> PP' t \<in> fmla"
  unfolding PP'_def by auto

lemma RR[simp,intro]: "t1 \<in> trm \<Longrightarrow> t2 \<in> trm \<Longrightarrow> RR t1 t2 \<in> fmla"
  by (auto simp: RR_def)

lemma [simp]: "n \<in> num \<Longrightarrow> subst (RR (Var yy) (Var xx)) n xx = RR (Var yy) n"
  by (simp add: RR_def2)
lemma [simp]: "m \<in> num \<Longrightarrow> n \<in> num \<Longrightarrow> subst (RR (Var yy) m) n yy = RR n m"
  by (simp add: RR_def2 subst2_fresh_switch)

(*  *)

(* The Rosser modification of the Godel formula *)
definition \<phi>R :: 'fmla where "\<phi>R \<equiv> diag (neg P')"

lemma \<phi>R[simp,intro!]: "\<phi>R \<in> fmla"
and
Fvars_\<phi>R[simp]: "Fvars \<phi>R = {}"  
  unfolding \<phi>R_def wrepr.PP_def by auto

lemma prv_\<phi>R_eqv: 
"prv (eqv \<phi>R (neg (PP' \<langle>\<phi>R\<rangle>)))"
  unfolding \<phi>R_def PP'_def using prv_diag_eqv[of "neg P'"] by simp

 
end (* context Rosser_Form *) 




end 
