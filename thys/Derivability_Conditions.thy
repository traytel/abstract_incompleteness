(* An abstract formulation and proof of the second incompleteness theorem, 
which assumes the provability predicate "prf" and its weak representation P, 
subject to Hilbert-Bernays-Loeb (HBL) conditions. 
(It does not assumes any notion of "proof of" predicate and its representation.)
Its proof is essentially performed in provability logic  *)

theory Derivability_Conditions imports Abstract_Representability
begin 

(* We assume all three derivability conditions: *)
locale Deriv_Cond = 
WRepr_Provability 
  var trm fmla Var FvarsT substT Fvars subst
  num
  eql cnj imp all exi 
  prv 
  enc 
  P
for 
var :: "'var set" and trm :: "'trm set" and fmla :: "'fmla set" 
and Var FvarsT substT Fvars subst  
and num 
and eql cnj imp all exi 
and prv
and enc ("\<langle>_\<rangle>")
and P
+
assumes 
(* The second and third Hilbert-Bernays-Loeb (HBL) derivability conditions: *)
HBL2: "\<And>\<phi> \<chi>. \<phi> \<in> fmla \<Longrightarrow> \<chi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> Fvars \<chi> = {} \<Longrightarrow> 
  prv (imp (cnj (PP \<langle>\<phi>\<rangle>) (PP \<langle>imp \<phi> \<chi>\<rangle>)) 
           (PP \<langle>\<chi>\<rangle>))"
and 
HBL3: "\<And>\<phi>. \<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> prv (imp (PP \<langle>\<phi>\<rangle>) (PP \<langle>PP \<langle>\<phi>\<rangle>\<rangle>))"
begin

(* Recall what the HBL1 condition says: *)
lemma HBL1_PP: "\<phi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> prv \<phi> \<Longrightarrow> prv (PP \<langle>\<phi>\<rangle>)" 
  unfolding PP_def using HBL1 .

(* The implicational form of HBL2 *)
lemma HBL2_imp: 
 "\<And>\<phi> \<chi>. \<phi> \<in> fmla \<Longrightarrow> \<chi> \<in> fmla \<Longrightarrow> Fvars \<phi> = {} \<Longrightarrow> Fvars \<chi> = {} \<Longrightarrow> 
  prv (imp (PP \<langle>imp \<phi> \<chi>\<rangle>) (imp (PP \<langle>\<phi>\<rangle>) (PP \<langle>\<chi>\<rangle>)))"
using HBL2 by (simp add: prv_cnj_imp prv_imp_com)

(* ... and its weaker, detached version: *)
lemma HBL2_imp2: 
assumes "\<phi> \<in> fmla" and "\<chi> \<in> fmla" "Fvars \<phi> = {}" "Fvars \<chi> = {}"
assumes "prv (PP \<langle>imp \<phi> \<chi>\<rangle>)"
shows "prv (imp (PP \<langle>\<phi>\<rangle>) (PP \<langle>\<chi>\<rangle>))"
using assms by (meson HBL2_imp PP enc imp num prv_imp_mp subsetCE) 


end (* Deriv_Cond *)
 
end 



