theory Abstract_First_Godel_Rosser imports Rosser_Formula 
begin 

context Rosser_Form 
begin

lemma NN_neg_unique_xx':
assumes [simp]:"\<phi> \<in> fmla"
shows 
"prv (imp (NN \<langle>\<phi>\<rangle> (Var xx'))  
          (eql \<langle>neg \<phi>\<rangle> (Var xx')))" (is "prv ?A")
using prv_subst[of yy _ "Var xx'", OF _ _ _ NN_neg_unique[of \<phi>]] by auto

lemma NN_imp_xx': 
assumes [simp]: "\<phi> \<in> fmla" "\<chi> \<in> fmla" 
shows "prv (imp (subst \<chi> \<langle>neg \<phi>\<rangle> xx')
                (all xx' (imp (NN \<langle>\<phi>\<rangle> (Var xx')) \<chi>)))"
proof-
  have 2: "prv (imp (eql \<langle>neg \<phi>\<rangle> (Var xx')) (imp (subst \<chi> \<langle>neg \<phi>\<rangle> xx') \<chi>))"  
   using prv_eql_subst_trm[of xx' \<chi> "\<langle>neg \<phi>\<rangle>" "Var xx'", simplified] .
  have 1: "prv (imp (subst \<chi> \<langle>neg \<phi>\<rangle> xx') (imp (eql \<langle>neg \<phi>\<rangle> (Var xx')) \<chi>))" 
     by (simp add: "2" prv_imp_com)
  have 0: "prv (imp (subst \<chi> \<langle>neg \<phi>\<rangle> xx') (imp (NN \<langle>\<phi>\<rangle> (Var xx')) \<chi>))" using 1 NN_neg_unique_xx'  
    by (smt NN Var assms enc eql imp in_num local.subst neg prv_imp_com prv_prv_imp_trans xx')
  show ?thesis apply(rule prv_all_imp_gen) by (auto simp: Fvars_subst 0)
qed

lemma godel_rosser_first_theEasyHalf: 
assumes c: "consistent"
shows "\<not> prv \<phi>R"
proof
  assume 0: "prv \<phi>R"
  then obtain "prf" where [simp]: "prf \<in> proof" and "prfOf prf \<phi>R" using prv_prfOf by auto
  hence 00: "prv (PPf (encPf prf) \<langle>\<phi>R\<rangle>)" using prfOf_PPf by auto  
  have "\<not> prv (neg \<phi>R)" using 0 c unfolding consistent_def3 by auto
  hence "\<forall> prf \<in> proof.  \<not> prfOf prf (neg \<phi>R)" using prv_prfOf by auto
  hence "\<forall> p \<in> num. prv (neg (PPf p \<langle>neg \<phi>R\<rangle>))" using not_prfOf_PPf PPf_encPf apply auto
    by (case_tac "p \<in> encPf ` proof") auto
  hence 1: "prv (all zz (imp (LLq (Var zz) (encPf prf)) (neg (PPf (Var zz) \<langle>neg \<phi>R\<rangle>))))"
    (* here use locale assumption about the order-like relation: *)   
    by (intro LLq_num) auto  
  have 11: "prv (RR (encPf prf) \<langle>\<phi>R\<rangle>)" apply(simp add: RR_def2 R_def)  
    apply(rule prv_all_congW[OF _ _ _ _ 1]) apply(auto intro!: prv_imp_monoL)
    using NN_imp_xx'[of \<phi>R "neg (PPf (Var zz) (Var xx'))", simplified] .
  have 3: "prv (cnj (PPf (encPf prf) \<langle>\<phi>R\<rangle>) (RR (encPf prf) \<langle>\<phi>R\<rangle>))" apply(rule prv_cnjI[OF _ _ 00 11]) by auto
  have "prv ((PP' \<langle>\<phi>R\<rangle>))" unfolding PP'_def P'_def apply simp
    apply(rule prv_exiI[of _ _ "encPf prf"]) using 3 by auto
  moreover have "prv (neg (PP' \<langle>\<phi>R\<rangle>))" using prv_eqv_prv[OF _ _ 0 prv_\<phi>R_eqv] by auto
  ultimately show False using c unfolding consistent_def3 by auto
qed

lemma godel_rosser_first_theHardHalf: 
assumes c: "consistent"
shows "\<not> prv (neg \<phi>R)"
proof
  assume 0: "prv (neg \<phi>R)"
  then obtain "prf" where [simp,intro!]: "prf \<in> proof" and pr: "prfOf prf (neg \<phi>R)" using prv_prfOf by auto
  define p where p: "p = encPf prf"
  have [simp,intro!]: "p \<in> num" unfolding p by auto
  have 11: "prv (PPf p \<langle>neg \<phi>R\<rangle>)" using pr prfOf_PPf unfolding p by auto
  have 1: "prv (NN \<langle>\<phi>R\<rangle> \<langle>neg \<phi>R\<rangle>)" using NN_neg by blast 

  have "\<not> prv \<phi>R" using 0 c unfolding consistent_def3 by auto
  from not_prv_prv_neg_PPf[OF _ _ this] 
  have 2: "\<forall> r \<in> num. prv (neg (PPf r \<langle>\<phi>R\<rangle>))" by auto 

  obtain P where P[simp,intro!]: "P \<subseteq>num" "finite P" 
  and 3: "prv (dsj (sdsj {eql (Var yy) r |r. r \<in> P}) (LLq p (Var yy)))" 
 (* here use the other locale assumption about the order-like relation: *)
    using LLq_num2 by auto  

  have "prv (imp (cnj (PPf (Var yy) \<langle>\<phi>R\<rangle>) (RR (Var yy) \<langle>\<phi>R\<rangle>)) fls)"
  proof(rule prv_dsj_cases[OF _ _ _ 3])
    {fix r assume r: "r \<in> P" hence rn[simp]: "r \<in> num" using P(1) by blast
     have "prv (imp (cnj (PPf r \<langle>\<phi>R\<rangle>) (RR r \<langle>\<phi>R\<rangle>)) fls)" 
        using 2 unfolding neg_def 
        by (metis FvarsT_num PPf RR rn \<phi>R all_not_in_conv cnj enc fls imp in_num prv_imp_cnj3L prv_imp_mp)
     hence "prv (imp (eql (Var yy) r) 
                (imp (cnj (PPf (Var yy) \<langle>\<phi>R\<rangle>) (RR (Var yy) \<langle>\<phi>R\<rangle>)) fls))"
     using prv_eql_subst_trm_id[of yy "cnj (PPf (Var yy) \<langle>\<phi>R\<rangle>) (RR (Var yy) \<langle>\<phi>R\<rangle>)" r,simplified]
     unfolding neg_def[symmetric]
     by (intro prv_neg_imp_imp_trans) auto
    }
    thus "prv (imp (sdsj {eql (Var yy) r |r. r \<in> P}) 
              (imp (cnj (PPf (Var yy) \<langle>\<phi>R\<rangle>) (RR (Var yy) \<langle>\<phi>R\<rangle>)) fls))"
    apply (intro prv_sdsj_imp) apply auto using Var P(1) eql by (simp add: set_rev_mp)
  next 
    let ?\<phi> = "all xx' (imp (NN \<langle>\<phi>R\<rangle> (Var xx')) (neg (PPf p (Var xx'))))" 
    have "prv (neg ?\<phi>)" apply(rule prv_imp_neg_allWI[where t = "\<langle>neg \<phi>R\<rangle>"])
    using 1 11 by (auto intro: prv_prv_neg_imp_neg) 
    hence 00: "prv (imp (LLq p (Var yy))
                       (imp (imp (LLq p (Var yy)) ?\<phi>) fls))" 
    unfolding neg_def[symmetric] by (intro prv_imp_neg_imp_neg_imp) auto      
    have "prv (imp (LLq p (Var yy)) 
              (imp (RR (Var yy) \<langle>\<phi>R\<rangle>) fls))" 
      unfolding neg_def[symmetric]
      apply(simp add: RR_def2 R_def)
      apply(intro prv_imp_neg_allI[where t = p]) 
      using 00 by (auto simp: neg_def)
    thus "prv (imp (LLq p (Var yy)) 
              (imp (cnj (PPf (Var yy) \<langle>\<phi>R\<rangle>) (RR (Var yy) \<langle>\<phi>R\<rangle>)) fls))"
    unfolding neg_def[symmetric] apply(intro prv_imp_neg_imp_cnjR ) by auto
  qed(auto, insert Var P(1) eql, simp_all add: set_rev_mp)
  hence "prv (neg (exi yy (cnj (PPf (Var yy) \<langle>\<phi>R\<rangle>) (RR (Var yy) \<langle>\<phi>R\<rangle>))))" 
    unfolding neg_def[symmetric] by (intro prv_neg_neg_exi) auto
  hence "prv (neg (PP' \<langle>\<phi>R\<rangle>))" unfolding PP'_def P'_def by simp
  hence "prv \<phi>R" using prv_\<phi>R_eqv by (meson PP' \<phi>R enc in_num neg prv_eqv_prv_rev)
  with `\<not> prv \<phi>R` show False using c unfolding consistent_def3 by auto
qed 

theorem godel_rosser_first: 
assumes "consistent"
shows "\<not> prv \<phi>R \<and> \<not> prv (neg \<phi>R)"
  using assms godel_rosser_first_theEasyHalf godel_rosser_first_theHardHalf by blast

theorem godel_rosser_first_ex: 
assumes "consistent"
shows "\<exists> \<phi>. \<phi> \<in> fmla \<and> \<not> prv \<phi> \<and> \<not> prv (neg \<phi>)" 
  using assms godel_rosser_first by (intro exI[of _ \<phi>R]) blast


end (* context Rosser_Form  *)

 

end 
